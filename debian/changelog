sugar-record-activity (102-1) unstable; urgency=medium

  [ upstream ]
  * New release.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Sep 2015 20:51:34 +0200

sugar-record-activity (100-3) unstable; urgency=medium

  * Relax watch file to cover any compression suffix.
  * Update package relations:
    + Fix depend on GStreamer-plugin plugins-base (not ffmpeg).
      See bug#790375.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 28 Jun 2015 20:30:09 -0500

sugar-record-activity (100-2) unstable; urgency=medium

  * Update package relations:
    + Fix stop recommend sugar.
    + Fix depend on python-sugar and python-sugar-toolkit.
    + Build-depend on unbranched python-sugar and python-sugar-toolkit.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 26 Jun 2015 11:32:10 -0500

sugar-record-activity (100-1) unstable; urgency=medium

  * Use cdbs upstream-tarball.mk snippet for get-orig-source target (not
    custom rules).
  * Update copyright info:
    + Add alternate git source URL.
    + Rewrite using copyright file format 1.0.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Fix Upstream-Name.
    + Extend coverage for myself.
    + Relicense packaging as GPL-3+.
    + Fix source URL to reference tarball releases.
  * Rewrite README.source to emphasize that control.in file is *not* a
    show-stopper for contributions, and refer to wiki page for details.
  * Bump debhelper compatibility level to 8.
  * Update package relations:
    + Bump primary branch to 0.104.
    + Suppress build-dependency on python-dev future-compatibly with
      cdbs 0.5.
    + Relax to build-depend unversioned on devscripts, python and
      debhelper: Needed versions satisfied even in oldstable.
    + Fix depend on python-libs gst0.10 gobject-2 cairo dbus rsvg.
    + Fix depend on gstreamer-libs alsa plugins-good ffmpeg x.
    + Fix depend explicitly on python.
    + Stop depend on python-lib cjson.
  * Let cdbs python-sugar.mk snippet replace COPYING file with symlink
    if identical to system shared file (instead of blindly removing it).
  * Stop permit copyright hint file to contain binary data: Serves no
    purpose - either suppress binary files or better extract data before
    copyright check.
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.
  * Declare compliance with Debian Policy 3.9.6.
  * Add myself as uploader.
  * Remove Neeraj Gupta as uploader: Thanks for your contributions,
    Neeraj.
  * Move packaging to Debian Sugar Team.
  * Modernize short and long description.
  * Suppress copyright check of PNG and WAV files.
  * Drop patch for activity.info file: Applied upstream.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 23 Jun 2015 15:53:05 -0500

sugar-record-activity (82-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Convert to dh_python2 (Closes: #617109).

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 09 Jul 2013 20:39:18 +0200

sugar-record-activity (82-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix activity.info file (Closes: #706018)

 -- Gaudenz Steinlin <gaudenz@soziologie.ch>  Tue, 23 Apr 2013 18:21:43 +0200

sugar-record-activity (82-1) unstable; urgency=low

  * Initial release. (Closes: #588993)

 -- Neeraj Gupta <neeraj@seeta.in>  Wed, 11 Aug 2010 11:02:30 -0400
